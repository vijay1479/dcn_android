package com.example.appsprotocol.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("F_NAME")
    @Expose
    private String fNAME;
    @SerializedName("L_NAME")
    @Expose
    private String lNAME;
    @SerializedName("ADDRESS")
    @Expose
    private String aDDRESS;
    @SerializedName("EMAIL")
    @Expose
    private String eMAIL;
    @SerializedName("ADHAR")
    @Expose
    private String aDHAR;
    @SerializedName("PAN")
    @Expose
    private String pAN;
    @SerializedName("GST")
    @Expose
    private String gST;
    @SerializedName("USER_NAME")
    @Expose
    private String uSERNAME;
    @SerializedName("PASSWORD")
    @Expose
    private String pASSWORD;
    @SerializedName("TYPE")
    @Expose
    private String tYPE;
    @SerializedName("OP_ID")
    @Expose
    private String oPID;
    @SerializedName("JOINING_DATE")
    @Expose
    private Object jOININGDATE;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getFNAME() {
        return fNAME;
    }

    public void setFNAME(String fNAME) {
        this.fNAME = fNAME;
    }

    public String getLNAME() {
        return lNAME;
    }

    public void setLNAME(String lNAME) {
        this.lNAME = lNAME;
    }

    public String getADDRESS() {
        return aDDRESS;
    }

    public void setADDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    public String getEMAIL() {
        return eMAIL;
    }

    public void setEMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    public String getADHAR() {
        return aDHAR;
    }

    public void setADHAR(String aDHAR) {
        this.aDHAR = aDHAR;
    }

    public String getPAN() {
        return pAN;
    }

    public void setPAN(String pAN) {
        this.pAN = pAN;
    }

    public String getGST() {
        return gST;
    }

    public void setGST(String gST) {
        this.gST = gST;
    }

    public String getUSERNAME() {
        return uSERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    public String getPASSWORD() {
        return pASSWORD;
    }

    public void setPASSWORD(String pASSWORD) {
        this.pASSWORD = pASSWORD;
    }

    public String getTYPE() {
        return tYPE;
    }

    public void setTYPE(String tYPE) {
        this.tYPE = tYPE;
    }

    public String getOPID() {
        return oPID;
    }

    public void setOPID(String oPID) {
        this.oPID = oPID;
    }

    public Object getJOININGDATE() {
        return jOININGDATE;
    }

    public void setJOININGDATE(Object jOININGDATE) {
        this.jOININGDATE = jOININGDATE;
    }

}