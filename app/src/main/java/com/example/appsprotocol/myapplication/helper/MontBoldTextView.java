package com.example.appsprotocol.myapplication.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class MontBoldTextView extends AppCompatTextView {
    public MontBoldTextView(Context context) {
        super(context);
        init();
    }

    public MontBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MontBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Montserrat-SemiBold.ttf");
        setTypeface(tf ,1);
    }
}
