package com.example.appsprotocol.myapplication;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.appsprotocol.myapplication.model.LoginResponse;
import com.example.appsprotocol.myapplication.utility.ApiClient;
import com.example.appsprotocol.myapplication.utility.ApiContract;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText etuser_name, etuser_pass;
    private Button btn_login;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private  void initView(){
        etuser_name = (EditText)findViewById(R.id.etuser_id);
        etuser_pass = (EditText)findViewById(R.id.etuser_pass);
        btn_login = (Button)findViewById(R.id.btn_login);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging...");
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId()==R.id.btn_login){
                    String user_name = etuser_name.getText().toString().trim();
                    String user_pass = etuser_pass.getText().toString().trim();
                    if (MyApplication.checkNetworkConnection(LoginActivity.this))
                        login(user_name,user_pass);
                    else
                        MyApplication.createAlertBuilder(LoginActivity.this,ApiContract.ERROR_INTERNET).show();
                }
            }
        });
    }

    private void login(String user_name, String user_pass){
        if (user_name.isEmpty()){
            etuser_name.setError("username is required..!!");
            etuser_name.requestFocus();
            return;
        }

        if (user_pass.isEmpty()){
            etuser_pass.setError("password is required..!!");
            etuser_pass.requestFocus();
            return;
        }
        showDialog();
        Retrofit retrofit = MyApplication.getInstance().getRetrofit();
        ApiClient client = retrofit.create(ApiClient.class);
        Call<ResponseBody> call = client.login(ApiContract.API_KEY,user_name,user_pass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                hideDialog();
                try {
                    String json = response.body().string();
                    Gson gson = new GsonBuilder().create();
                    LoginResponse loginResponse = gson.fromJson(json,LoginResponse.class);
                    if (loginResponse.getStatus().equalsIgnoreCase("1"))
                        MyApplication.createAlertBuilder(LoginActivity.this,"Login Successful..").show();
                    else {
                        String msg = ApiContract.error_map.get((Integer) loginResponse.getErrors().getCode());
                        MyApplication.createAlertBuilder(LoginActivity.this, msg).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideDialog();
            }
        });
    }

    private void showDialog(){
        if (progressDialog != null)
            progressDialog.show();
    }

    private void hideDialog(){
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

}
