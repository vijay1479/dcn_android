package com.example.appsprotocol.myapplication.utility;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by AppsProtocol on 8/3/2018.
 */

public interface ApiClient {

    @FormUrlEncoded
    @POST("lineman/lineman_login")
    Call<ResponseBody> login(@Field("api_key") String api_key, @Field("USER_NAME") String user_name, @Field("PASSWORD") String password);
}
