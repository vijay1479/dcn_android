package com.example.appsprotocol.myapplication.utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AppsProtocol on 8/3/2018.
 */

public class ApiContract {
    public static final String APP_BASE_URL = "http://178.128.92.47/api/";
    public static final String API_KEY = "1234";
    public static final String APP_PREF = "lineman_app";
    public static final Map<Integer,String> error_map = new HashMap<>();
    public static final String ERROR_INTERNET = "Check Your Internet Connection";

    ApiContract(){
        error_map.put(100,"invalid api key");
        error_map.put(101,"required field is not set");
        error_map.put(102,"invalid credentials");
    }

    public String getErrorCode(int code){
        return error_map.get(code);
    }
}
